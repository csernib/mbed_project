#ifndef CURRENTTIMESERVICE_H
#define CURRENTTIMESERVICE_H

#include "mbed.h"
#include "ble/BLE.h"


extern DigitalOut led1;
extern Serial usb;

extern const uint8_t  LedServiceUUID[];
extern const uint8_t  LedCharacteristicUUID[];


class LedService
{
public:
    LedService(BLE &ble)
        : rMyBle(ble)
        , myLedData()
        , myLedCharacteristics(LedCharacteristicUUID,
                               myLedData,
                               LED_DATA_SIZE, LED_DATA_SIZE,
                               GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ |
                               GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE)
    {
        myLedCharacteristics.setReadAuthorizationCallback(this, &LedService::readAuthorizationCallback);

        GattCharacteristic* charTable[] = { &myLedCharacteristics };
        GattService ledService(LedServiceUUID, charTable, sizeof(charTable) / sizeof(GattCharacteristic*));

        ble.addService(ledService);
        ble.onDataWritten(this, &LedService::onDataWritten);
    }

protected:
    void onDataWritten(const GattWriteCallbackParams* params)
    {
        if (params->handle == myLedCharacteristics.getValueAttribute().getHandle())
        {
            if (!strcmp((char*)params->data, "on"))
                led1 = 1;
            else if (!strcmp((char*)params->data, "off"))
                led1 = 0;
                
            usb.printf("led written: %s\r\n", params->data);
        }
    }

    void readAuthorizationCallback(GattReadAuthCallbackParams *params)
    {
        strcpy((char*)myLedData, led1 ? "on" : "off");
        usb.printf("led read: %s\r\n", myLedData);
    
        
        // See GattCharacteristic::authorizeRead
        // "If the read is approved, a new value can be provided by setting the params->data pointer and params->len fields."
        params->data = myLedData;
        params->len = LED_DATA_SIZE;
        params->authorizationReply = AUTH_CALLBACK_REPLY_SUCCESS;
    }

    enum { LED_DATA_SIZE = 4 };

    BLE &rMyBle;
    uint8_t myLedData[LED_DATA_SIZE];
    GattCharacteristic myLedCharacteristics;
};

#endif /* CURRENTTIMESERVICE_H */
